"use strict"

function randomArray(length) {
    var array = [];
    for (var i = 0; i < length; i++) {
        array[i] = Math.floor(Math.random() * 10);
    }
    return array;
}

function arrayChange(length) {
    let arrayIndex = Math.floor(length / 2) - 1;
    let sortStart = arrayIndex + (length % 2) + 1;
    let operationsNumber = Math.floor(length / 4);
    let array = randomArray(length);
    console.log(array.toString());
    for (let i = 0; i < operationsNumber; i++) {
        let x = array[i];
        array[i] = array[arrayIndex - i];
        array[arrayIndex - i] = x;
        x = array[sortStart + i];
        array[sortStart + i] = array[array.length - (i + 1)];
        array[array.length - (i + 1)] = x;
    }
    console.log(array.toString());
}

console.log("Тест 1 : ")
arrayChange(6);
console.log("Тест 2 : ");
arrayChange(5);
console.log("Тест 3 : ");
arrayChange(9);