'use strict'

class Person {
    var
    married;

    constructor(name) {
        this.name = name;
        married = false;
    }

    sayHello() {
        console.log("hi, my name is " + this.name);
        console.log(married);
    }
}

class Man extends Person {
    constructor(name) {
        super(name);
    }

    married(person) {
        if (person instanceof Woman) {
            married = true
        } else {
            console.log("Ай ай ай");
        }
    }

    friends(person) {
        if (person instanceof
            Man
        ) {
            console.log("Бухашка")
            return true;
        }
        else {
            console.log("фу фу фу фу");
            return false;
        }
    }
}

class Woman extends Person {
    constructor(name) {
        super(name);
    }

    married(person) {
        if (person instanceof man) {
            married = true
        } else {
            console.log("Ай ай ай");
        }
    }

    friends(person) {
        if (person instanceof
            Woman
        ) {
            console.log("Драка")
            return false;
        }
        else {
            console.log("Все норм");
            return true;
        }
    }
}

{
    var man1 = new Man("Сергей");
    var man2 = new Woman("Маша");
    var man3 = new Man("Валера");
    man1.friends(man2);
    man1.friends(man3);
    man1.sayHello();
    man2.sayHello();
    man3.sayHello();
    man2.married(man1);
    man3.married(man1);
}

